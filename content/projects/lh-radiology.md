+++
title = "Radiology Information systems"
+++

Radiology information system aids in managing the radiology department electronically. 

## Features
1. Image tracking:- It helps the providers in tarcking individual films and other associated data.
2. Patient management:- RIS aids in tracking the patient's work flow within the radiology department
3. Patient tracking:- RIS system helps in tracking the patients complete radiology history and synchronize it with past, future appointments.
4. Results reporting:- RIS is capable of generating reports of an individual patient, group of patients or for particular procedure
5. Scheduling:- RIS helps in making appointments for both inpatients and outpatients

## RADIOLOGY MODULE
Our Radiology suite is a customized version of LibreHealth Toolkit with additional tools for radiology and imaging professionals.

## Vision
The OpenMRS radiology module aims at adding capabitlities of a Radiology Information system (RIS)onto OpenMRS.

## Objectives
Develop a module capable of manage infomation in a radiology department.
1)Identify radiology's department workflow according to radiology experts.
2)Design an architecture that supports basic radiology's department services.
3)Implement the module as suggested by architecture, by using  free and open source software.
4)Satisfy modules' quality parameters set by community.

## Features
Current set of features include placing radiology orders in openmrs, configure a set of concept classes to define the orderable imaging procedure at the healthacre facility, configure DICOM web viewer like oviyam or weasis to open the ordered radiology studies from within OpenMRS and create radiology report once a radiology order is completed.


 Maintainer: Judy Gichoya
 
 Official Code Repository: https://gitlab.com/librehealth/lh-toolkit
 
 Discussion Forum: https://forums.librehealth.io/c/projects/lh-rad
