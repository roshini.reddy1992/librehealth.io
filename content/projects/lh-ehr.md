+++
title = "LibreHealth EHR"
+++

## 
The LibreHealth EHR applicaiton is a clinically-focused electronic health record (EHR) system designed to be both easy to use “out of the box” and also customizable for use in a variety of health care settings. It builds on the strength of the LibreHealth Toolkit, and adapts many of the proven user experiences built over many years in the OpenEMR system. It is designed to be a modern, easy-to-use experience for health care professionals in their daily work.


Maintainer: Tony McCormick

Official Code Repository: https://github.com/LibreEHR/LibreEH

Discussion Forum: https://forums.librehealth.io/c/projects/lh-emr