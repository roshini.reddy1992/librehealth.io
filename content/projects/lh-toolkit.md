+++
title = "LibreHealth Toolkit"
+++


Our Toolkit software serves as the foundational API and data model for many other Health IT applications. It will be frequently (but not always) utilized as an upstream product for buidling other LibreHealth software. Toolkit is a fork of the OpenMRS Platform, and builds on its reliability while improving performance and ease of installation and use.

## Features
1.Community-driven EHR toolkit:- Built by and for the community. Your contributions help shale the product . Please look at ways to contribute on the top of the page
	
2.Flexible and powerful :- You can build and EHR application that is suitable to your healthcare needs quickly
	
3.Our mission:-  Our product is built for the clinician, nurse and midwife at the remotest corner of the world, as well as the largest, cutting edge health systems of the world. Depends on how you choose to use the toolkit
	
4.Multi-platform and scalable:- Toolkit runs on windows, Linux and OSX. It runs on MySQL, PostgreSQL. It works on mobile (with mUzima), desktop and laptops.
	
5.Transparent and open:- We welcome a diverse community of individuals who want to improve healthcare around the world
	
6.Multi-Lingual user interface:- LibreHealth Toolkit is available in 8 different languages such as English, Spansish,Portuguese, Sinhala, French, Hindi, German, Persian


Maintainer: Saptarshi Purkayastha

Official Code Repository: https://gitlab.com/librehealth/lh-toolkit
 
Discussion Forum: https://forums.librehealth.io/c/projects/lh-toolkit

